
## Other Configuration Data Stores

### Specialized Dynamic Stores

In addition to these methods that can be accomplished within GitLab, frequently environment configuration data is stored in specialized storage such as AWS Systems Manager Parameter Store or Hashicorp Consul. When these are employed, it makes tracking configuration changes with GIT more difficult. To restore that capability, one can create code that updates the parameter store in GitLab and then run CD automation to push it to a dynamic configuration storage engine.

The closest GitLab comes to these engines is the examples in [Complex Config Data Using CI Variables](https://gitlab.com/guided-explorations/cfg-data/complex-config-data-using-ci-variables) which provide dynamic inheritance and environment scoping. You can also see a fully converged view of the data in CI/CD Variables in each project. An explanation of the art of the possible using this project structure is at Minute 19 of this video [Solution Architecture: GitOps Managed DevOps Environments for Your Developer Platform](https://youtu.be/HM7TxtF1XrE?t=1132). The first 19 minutes talk about GitLab's Environments as Code capabilities.

### Secret Stores

While secrets are really just a special case of configuration data, there are specialized engines for them such as Hashicorp Vault and AWS Secrets Manager. Some regular parameter stores can handle secrets as well - such as KMS encrypted parameters in AWS Systems Manager Parameter Store.

GitLab has direct CI integration with Hashicorp Vault, GCP Secret Manager and Azure Key Vault. AWS Secrets Manager support is planned and GitLab is also planning native secrets management.